Pong - Cube - Realsense Bridge:

This project code allows a Computer using a Realsense F200 to detect hands and joints using modified 2016 R2 Realsense SDK for windows sample code & libraries to detect the position of human hands placed in front of the camera and send them via usb serial to an Arduino Mega, where it is parsed and relayed to Chris Biddle's LED Cube.


======================================================================================

The Realsense part of the source, in directory [nuc/FF_HandsConsole] is the Realsense SDK Sample project FF_HandsConsole modified to:
- Ignore joints other than the center of the hand
- Extract the coordinates of the center of hand and parse into a round integer coordinate
- Establish a usb serial connection to a connected device (Arduino Mega here)
- Send coordinates for the center of any hands viewed 'firehose style' to the usb serial device.

The Realsense portion consists of a Visual Studio 2015 project, and depends on teh Intel Realsense 2016 R2 SDK being installed on a windows computer.

A pre-compiled binary [nuc/SendHandCenterCoordToArduino-COM3.exe] is included that, presuming a Realsense R200 camera is connected to a windows 10 computer with an Intel 5th gen i series processor or newer and all neccesary drivers installed will send hand coordinate data to serial port COM3.  The COM number *can* be changed by editing the binary directly as it is stored in plain text, though care must be used to use an editor that will not add windows-style line endings.

======================================================================================

The arduino sketch portion of the source, in directory [arduino], consists of one file named [PongCubeRealsenseBridge.ino].  This program:
- Receives serial data over the usb serial port on the Arduino Mega
- Parses the serial data into integer coordinate values
- Validates the data to ensure that it lies within the pong game coordinate system
- Tracks the Max/Min coordinate values received from the usb serial line and scales the current received values accordingly (a dynamic callibration feature)
- Relays the scaled values out of the Arduin Mega's 2nd serial line (wires), presumably to an arduino connected to an LED Cube.

