#include <Wire.h>
#include "rgb_lcd.h"

//timeout in seconds
#define TIMEOUT            10
#define INTERRUPT_SEND_PIN 2
#define DEMOMODE           B10110000
#define SPONGMODE          B10000000
#define XMSB               B00000000
#define YMSB               B00010000

byte playMode = DEMOMODE;
byte axcoord  = B00000000;
byte aycoord  = B00010000;
byte bxcoord  = B00100000;
byte bycoord  = B00110000;

unsigned long lastRxTimestamp = 0;
unsigned long timeout         = TIMEOUT * 1000;

bool nonce = true;

rgb_lcd lcd;

void setup() {
  pinMode( INTERRUPT_SEND_PIN, OUTPUT );
  digitalWrite( INTERRUPT_SEND_PIN, LOW );
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // initialize the serial communications:
  Serial.begin(9600);
  Serial1.begin(9600); // mega   19 rx     18 tx
}

void loop()
{
  if (Serial.available())
  {
    delay(100);
    while (Serial.available() > 0)
    {
      String val = Serial.readStringUntil('\0');
      playMode = SPONGMODE;
      if (val[0] == 'X') {
        
        if ((val[1] == '0') && (val[2] == '0')) {
          axcoord = XMSB | B00001111;
        }
        else if ((val[1] == '0') && (val[2] == '1')) {
          axcoord = XMSB | B00001110;
        }
        else if ((val[1] == '0') && (val[2] == '2')) {
          axcoord = XMSB | B00001101;
        }
        else if ((val[1] == '0') && (val[2] == '3')) {
          axcoord = XMSB | B00001100;
        }
        else if ((val[1] == '0') && (val[2] == '4')) {
          axcoord = XMSB | B00001011;
        }
        else if ((val[1] == '0') && (val[2] == '5')) {
          axcoord = XMSB | B00001010;
        }
        else if ((val[1] == '0') && (val[2] == '6')) {
          axcoord = XMSB | B00001001;
        }
        else if ((val[1] == '0') && (val[2] == '7')) {
          axcoord = XMSB | B00001000;
        }
        else if ((val[1] == '0') && (val[2] == '8')) {
          axcoord = XMSB | B00000111;
        }
        else if ((val[1] == '0') && (val[2] == '9')) {
          axcoord = XMSB | B00000110;
        }
        else if ((val[1] == '1') && (val[2] == '0')) {
          axcoord = XMSB | B00000101;
        }
        else if ((val[1] == '1') && (val[2] == '1')) {
          axcoord = XMSB | B00000100;
        }
        else if ((val[1] == '1') && (val[2] == '2')) {
          axcoord = XMSB | B00000011;
        }
        else if ((val[1] == '1') && (val[2] == '3')) {
          axcoord = XMSB | B00000010;
        }
        else if ((val[1] == '1') && (val[2] == '4')) {
          axcoord = XMSB | B00000001;
        }
        else if ((val[1] == '1') && (val[2] == '5')) {
          axcoord = XMSB | B00000000;
        }
      }
      else {
        
        if ((val[1] == '0') && (val[2] == '0')) {
          aycoord = YMSB | B00000000;
        }
        else if ((val[1] == '0') && (val[2] == '1')) {
          aycoord = YMSB | B00000001;
        }
        else if ((val[1] == '0') && (val[2] == '2')) {
          aycoord = YMSB | B00000010;
        }
        else if ((val[1] == '0') && (val[2] == '3')) {
          aycoord = YMSB | B00000011;
        }
        else if ((val[1] == '0') && (val[2] == '4')) {
          aycoord = YMSB | B00000100;
        }
        else if ((val[1] == '0') && (val[2] == '5')) {
          aycoord = YMSB | B00000101;
        }
        else if ((val[1] == '0') && (val[2] == '6')) {
          aycoord = YMSB | B00000110;
        }
        else if ((val[1] == '0') && (val[2] == '7')) {
          aycoord = YMSB | B00000111;
        }
        else if ((val[1] == '0') && (val[2] == '8')) {
          aycoord = YMSB | B00001000;
        }
        else if ((val[1] == '0') && (val[2] == '9')) {
          aycoord = YMSB | B00001001;
        }
        else if ((val[1] == '1') && (val[2] == '0')) {
          aycoord = YMSB | B00001010;
        }
        else if ((val[1] == '1') && (val[2] == '1')) {
          aycoord = YMSB | B00001011;
        }
        else if ((val[1] == '1') && (val[2] == '2')) {
          aycoord = YMSB | B00001100;
        }
        else if ((val[1] == '1') && (val[2] == '3')) {
          aycoord = YMSB | B00001101;
        }
        else if ((val[1] == '1') && (val[2] == '4')) {
          aycoord = YMSB | B00001110;
        }
        else if ((val[1] == '1') && (val[2] == '5')) {
          aycoord = YMSB | B00001111;
        }
      }
      digitalWrite( INTERRUPT_SEND_PIN, HIGH );
      delay( 1 );
      digitalWrite( INTERRUPT_SEND_PIN, LOW );
      Serial1.write(playMode);Serial1.flush();
      Serial1.write(axcoord);Serial1.flush();
      Serial1.write(aycoord);Serial1.flush();
      
      lcd.clear();
      String xstr = String(axcoord, BIN);
      String ystr = String(aycoord, BIN);
      lcd.setCursor(0, 0);
      lcd.print("X: " + xstr);
      lcd.setCursor(0, 1);
      lcd.print("Y: " + ystr);
      
      //Serial1.write(bxcoord);Serial1.flush();
      //Serial1.write(bycoord);Serial1.flush();
      nonce = true;
    }
    lastRxTimestamp = millis();
  }
  if (((unsigned long) (millis() - lastRxTimestamp) >= timeout) && nonce) {
    playMode = DEMOMODE;
    digitalWrite( INTERRUPT_SEND_PIN, HIGH );
    delay( 1 );
    digitalWrite( INTERRUPT_SEND_PIN, LOW );
    Serial1.write(playMode);Serial1.flush();
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("DEMO MODE");
    nonce = false;
  }
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/
