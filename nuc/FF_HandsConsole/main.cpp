﻿#include <windows.h>
#include <iostream>

#include "pxcsensemanager.h"
#include "pxcmetadata.h"
#include "service/pxcsessionservice.h"

#include "pxchandmodule.h"
#include "pxchanddata.h"
#include "pxchandconfiguration.h"
#include "pxccursordata.h"
#include "pxccursorconfiguration.h"
#include "pxchandcursormodule.h"

#include "Definitions.h"
#include "timer.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include "SerialPort.h"

#include <cmath>

#define XMAX 15
#define YMAX 15
#define ZMAX 24

//include <unistd.h>


//include "stdafx.h"



bool g_live = true;	    // true - Working in live camera mode, false - sequence mode
bool g_gestures = false;	// Writing gesture data to console output
bool g_alerts = false;	// Writing alerts data to console output
bool g_skeleton = true;		// Enable Full hand tracking mode
bool g_default = true;		// Writing hand type to console output
bool g_stop = false;	// user closes application
bool g_cursor = false;	// Enable Cursor tracking mode
bool g_engagement = false;	// Enable Cursor engagement mode
bool g_Info = false;	// Enable Cursor / Skeleton Information
bool g_timer = false;    // Enable FPS output

double XL = 0;
double XH = 0;
double YL = 0;
double YH = 0;
double ZL = 0;
double ZH = 0;

std::wstring g_sequencePath;

PXCSession *g_session;
PXCSenseManager *g_senseManager;
PXCHandModule *g_handModule;
PXCHandData *g_handDataOutput;
PXCHandConfiguration *g_handConfiguration;
PXCHandCursorModule *g_handCursorModule;
PXCCursorData *g_cursorDataOutput;
PXCCursorConfiguration *g_cursorConfiguration;

void releaseAll();
unsigned count_backoff = 0;

//String for getting the output from arduino
char output[MAX_DATA_LENGTH];

/*Portname must contain these backslashes, and remember to
replace the following com port*/
char *port_name = "\\\\.\\COM3";

//String for incoming data
char incomingData[MAX_DATA_LENGTH];

BOOL CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
		// Handle the CTRL-C signal. 
	case CTRL_C_EVENT:

		// confirm that the user wants to exit. 
	case CTRL_CLOSE_EVENT:
		g_stop = true;
		Sleep(1000);
		releaseAll();
		return(TRUE);

	default:
		return FALSE;
	}
}

void main(int argc, const char* argv[])
{

	SerialPort arduino(port_name);
	if (arduino.isConnected()) std::cout << "Connection Established" << std::endl;
	else std::cout << "ERROR, check port name";

	// Measuring FPS
	FPSTimer timer;

	Definitions::appName = argv[0];

	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);
	if (argc < 2)
	{
		g_live = true;
		std::printf("Tip: Run FF_HandsConsole.exe -help for additional parameters\n");
	}

	if (argc == 2)
	{
		if (strcmp(argv[1], "-help") == 0)
		{
			Definitions::WriteHelpMessage();
			return;
		}
	}

	// Setup
	g_session = PXCSession::CreateInstance();
	if (!g_session)
	{
		std::printf("Failed Creating PXCSession\n");
		return;
	}

	g_senseManager = g_session->CreateSenseManager();
	if (!g_senseManager)
	{
		releaseAll();
		std::printf("Failed Creating PXCSenseManager\n");
		return;
	}

	int moduleCount = 0;

	// Iterating input parameters
	for (int i = 1; i<argc; i++)
	{
		if (strcmp(argv[i], "-live") == 0)
		{
			g_live = true;
		}

		if (strcmp(argv[i], "-seq") == 0)
		{
			if (argc == i + 1)
			{
				releaseAll();
				std::printf("Error: Sequence path is missing\n");
				return;
			}
			g_live = false;
			std::string tmp(argv[i + 1]);
			i++;
			g_sequencePath.clear();
			g_sequencePath.assign(tmp.begin(), tmp.end());
			continue;
		}

		if (strcmp(argv[i], "-skeleton") == 0)
		{
			++moduleCount;
			g_skeleton = true;
			g_cursor = false;
		}
		if (strcmp(argv[i], "-cursor") == 0)
		{
			++moduleCount;
			g_cursor = true;
			g_skeleton = false;
		}

		if (strcmp(argv[i], "-fps") == 0)
		{
			g_timer = true;
		}

		if (strcmp(argv[i], "-gestures") == 0)
		{
			g_gestures = true;
		}

		if (strcmp(argv[i], "-alerts") == 0)
		{
			g_alerts = true;
		}

		if (strcmp(argv[i], "-info") == 0)
		{
			g_Info = true;
		}
		if (strcmp(argv[i], "-engagement") == 0)
		{
			g_engagement = true;
		}
		//g_Info = true;

	}

	if (moduleCount == 2)
	{
		std::printf("Cannot enable both cursor and skeleton\n");
		return;
	}

	if (g_skeleton)
	{
		if (g_cursor)
		{
			std::printf("Cannot enable both cursor and skeleton\n");
			return;
		}

		if (g_senseManager->EnableHand(0) != PXC_STATUS_NO_ERROR)
		{
			releaseAll();
			std::printf("Failed Enabling Hand Module\n");
			return;
		}

		g_handModule = g_senseManager->QueryHand();
		if (!g_handModule)
		{
			releaseAll();
			std::printf("Failed Creating PXCHandModule\n");
			return;
		}



		g_handDataOutput = g_handModule->CreateOutput();
		if (!g_handDataOutput)
		{
			releaseAll();
			std::printf("Failed Creating PXCHandData\n");
			return;
		}

		g_handConfiguration = g_handModule->CreateActiveConfiguration();
		if (!g_handConfiguration)
		{
			releaseAll();
			std::printf("Failed Creating PXCHandConfiguration\n");
			return;
		}

		g_handConfiguration->SetTrackingMode(PXCHandData::TRACKING_MODE_FULL_HAND);

		if (g_gestures)
		{
			std::printf("-Gestures Are Enabled-\n");
			g_handConfiguration->EnableAllGestures();
		}

		if (g_alerts)
		{
			std::printf("-Alerts Are Enabled-\n");
			g_handConfiguration->EnableAllAlerts();
		}

		// Apply configuration setup
		g_handConfiguration->ApplyChanges();

		std::printf("-Skeleton Information Enabled-\n");
	}

	if (g_cursor)
	{
		if (g_skeleton)
		{
			std::printf("Cannot enable both cursor and skeleton\n");
			return;
		}


		if (g_senseManager->EnableHandCursor(0) != PXC_STATUS_NO_ERROR)
		{
			releaseAll();
			std::printf("Failed Enabling Hand Cursor Module\n");
			return;
		}

		g_handCursorModule = g_senseManager->QueryHandCursor();
		if (!g_handCursorModule)
		{
			releaseAll();
			std::printf("Failed Creating PXCHandCursorModule\n");
			return;
		}

		g_cursorDataOutput = g_handCursorModule->CreateOutput();
		if (!g_cursorDataOutput)
		{
			releaseAll();
			std::printf("Failed Creating PXCCursorData\n");
			return;
		}

		g_cursorConfiguration = g_handCursorModule->CreateActiveConfiguration();
		if (!g_cursorConfiguration)
		{
			releaseAll();
			std::printf("Failed Creating PXCCursorConfiguration\n");
			return;
		}

		if (g_gestures)
		{
			std::printf("-Gestures Are Enabled-\n");
			g_cursorConfiguration->EnableAllGestures();

		}
		if (g_alerts)
		{
			std::printf("-Alerts Are Enabled-\n");
			g_cursorConfiguration->EnableAllAlerts();
		}
		if (g_engagement)
		{
			std::printf("-Cursor engagement Enabled-\n");
			g_cursorConfiguration->EnableEngagement(true);
		}

		// Apply configuration setup
		g_cursorConfiguration->ApplyChanges();
		std::printf("-Cursor Information Enabled-\n");
	}

	// run sequences as fast as possible
	if (!g_live)
	{
		if (g_senseManager->QueryCaptureManager()->SetFileName(g_sequencePath.c_str(), false) != PXC_STATUS_NO_ERROR)
		{
			releaseAll();
			std::printf("Error: Invalid Sequence/ Sequence path\n");
			return;
		}
		g_senseManager->QueryCaptureManager()->SetRealtime(false);
	}

	pxcI32 numOfHands = 0;

	// First Initializing the sense manager
	if (g_senseManager->Init() == PXC_STATUS_NO_ERROR)
	{
		std::printf("\nPXCSenseManager Initializing OK\n========================\n");

		std::printf("-------------------------------- count_backofff: %d\n", count_backoff);
		// Acquiring frames from input device
		while (g_senseManager->AcquireFrame(true) == PXC_STATUS_NO_ERROR && !g_stop)
		{

			if (g_skeleton && (count_backoff == 0))
			{
				//std::printf("Running g_skeleton routine\n");
				// Get current hand outputs
				if (g_handDataOutput->Update() == PXC_STATUS_NO_ERROR)
				{
					//std::printf("BEFORE Running g_skeleton routine\n");
					// Display joints
					//if(g_Info)
					//if(count_backoff == 0)
					//{
					//std::printf("Running g_Info routine\n");
					PXCHandData::IHand *hand;
					PXCHandData::JointData jointData;
					for (int i = 0; i < g_handDataOutput->QueryNumberOfHands(); ++i)
					{
						g_handDataOutput->QueryHandData(PXCHandData::ACCESS_ORDER_BY_TIME, i, hand);
						std::string handSide = "Unknown Hand";
						handSide = hand->QueryBodySide() == PXCHandData::BODY_SIDE_LEFT ? "Left Hand" : "Right Hand";

						//std::printf("%s\n==============\n",handSide.c_str());
						for (int j = 0; j < 22; ++j)
						{
							if (hand->QueryTrackedJoint((PXCHandData::JointType)j, jointData) == PXC_STATUS_NO_ERROR)
							{
								char * desired = "JOINT_CENTER\0";
								if (strcmp(Definitions::JointToString((PXCHandData::JointType)j).c_str(), desired) == 0)
								{
									//std::printf(" Found it!:  ");01111111111111111111111111.....................0.
									//std::printf("     %s)\tX: %f, Y: %f, Z: %f \n", Definitions::JointToString((PXCHandData::JointType)j).c_str(), jointData.positionWorld.x, jointData.positionWorld.y, jointData.positionWorld.z);
									unsigned int xc = 0;
									unsigned int yc = 0;
									unsigned int zc = 0;
									double xraw = jointData.positionWorld.x;
									double yraw = jointData.positionWorld.y;
									double zraw = jointData.positionWorld.z;
									if (xraw < XL) XL = xraw;
									if (xraw > XH) XH = xraw;
									if (yraw < YL) YL = yraw;
									if (yraw > YH) YH = yraw;
									if (zraw < ZL) ZL = zraw;
									if (zraw > ZH) ZH = zraw;
									double xrange = XH - XL;
									double yrange = YH - YL;
									double zrange = ZH - ZL;
									double xshifted = xraw + ((-1)*XL);
									double yshifted = yraw + ((-1)*YL);
									double zshifted = zraw + ((-1)*ZL);
									double xsc = (xshifted * XMAX) / xrange;
									double ysc = (yshifted * YMAX) / yrange;
									double zsc = (zshifted * ZMAX) / zrange;
									unsigned int xu = static_cast<unsigned int> (xsc);
									unsigned int yu = static_cast<unsigned int> (ysc);
									unsigned int zu = static_cast<unsigned int> (zsc);

									if (arduino.isConnected()) {
										std::printf(" x: %d | y: %d | z: %d\n", xu, yu, zu);
										std::string xstr = "X00";
										xstr[1] = (unsigned char)(xu > 9) ? (((xu - (xu % 10)) / 10) + 48) : 48;
										xstr[2] = (unsigned char)((xu % 10) + 48);
										std::string ystr = "Y00";
										ystr[1] = (unsigned char)(yu > 9) ? (((yu - (yu % 10)) / 10) + 48) : 48;
										ystr[2] = (unsigned char)((yu % 10) + 48);
										std::string zstr = "Z00";
										zstr[1] = (unsigned char)(zu > 9) ? (((zu - (zu % 10)) / 10) + 48) : 48;
										zstr[2] = (unsigned char)((zu % 10) + 48);
										arduino.writeSerialPort((char*)xstr.c_str(), 4);
										arduino.writeSerialPort((char*)ystr.c_str(), 4);
										/*TO ENABLE Z*/ //arduino.writeSerialPort((char*)zstr.c_str(), 4);

										/*xc = unsigned int ((63 * sx) / 15);
										yc = unsigned int ((31 * sy) / 15);
										std::string xstr = "X00";
										xstr[1] = (unsigned char)(xc > 9) ? (((xc - (xc % 10)) / 10) + 48) : 48;
										xstr[2] = (unsigned char)((xc % 10) + 48);
										std::string ystr = "Y00";
										ystr[1] = (unsigned char)(yc > 9) ? (((yc - (yc % 10)) / 10) + 48) : 48;
										ystr[2] = (unsigned char)((yc % 10) + 48);
										arduino.writeSerialPort((char*)xstr.c_str(), 4);
										arduino.writeSerialPort((char*)ystr.c_str(), 4);
										//std::printf("  POS: out <%f,%f>  \n", jointData.positionWorld.x, jointData.positionWorld.y);
										std::printf(" xlow: %f | xhigh: %f | ylow: %f | yhigh: %f\n", xlow, xhigh, ylow, yhigh);*/
										/*
										if (sx < 0.5) {
										xc = 0;
										arduino.writeSerialPort("X00", 3);
										}
										else if (sx < 1.0) {
										xc = 1;
										arduino.writeSerialPort("X01\0", 4);
										}
										else if (sx < 1.5) {
										xc = 2;
										arduino.writeSerialPort("X02\0", 4);
										}
										else if (sx < 2.0) {
										xc = 3;
										arduino.writeSerialPort("X03\0", 4);
										}
										else if (sx < 2.5) {
										xc = 4;
										arduino.writeSerialPort("X04\0", 4);
										}
										else if (sx < 3.0) {
										xc = 5;
										arduino.writeSerialPort("X05\0", 4);
										}
										else if (sx < 3.5) {
										xc = 6;
										arduino.writeSerialPort("X06\0", 4);
										}
										else if (sx < 4.0) {
										xc = 7;
										arduino.writeSerialPort("X07\0", 4);
										}
										else if (sx < 4.5) {
										xc = 8;
										arduino.writeSerialPort("X08\0", 4);
										}
										else if (sx < 5.0) {
										xc = 9;
										arduino.writeSerialPort("X09\0", 4);
										}
										else if (sx < 5.5) {
										xc = 10;
										arduino.writeSerialPort("X10\0", 4);
										}
										else if (sx < 6.0) {
										xc = 11;
										arduino.writeSerialPort("X11\0", 4);
										}
										else if (sx < 6.5) {
										xc = 12;
										arduino.writeSerialPort("X12\0", 4);
										}
										else if (sx < 7.0) {
										xc = 13;
										arduino.writeSerialPort("X13\0", 4);
										}
										else if (sx < 7.5) {
										xc = 14;
										arduino.writeSerialPort("X14\0", 4);
										}
										else {
										xc = 15;
										arduino.writeSerialPort("X15\0", 4);
										}



										if (sy < 0.5) {
										yc = 0;
										arduino.writeSerialPort("Y00\0", 4);
										}
										else if (sy < 1.0) {
										yc = 1;
										arduino.writeSerialPort("Y01\0", 4);
										}
										else if (sy < 1.5) {
										yc = 2;
										arduino.writeSerialPort("Y02\0", 4);
										}
										else if (sy < 2.0) {
										yc = 3;
										arduino.writeSerialPort("Y03\0", 4);
										}
										else if (sy < 2.5) {
										yc = 4;
										arduino.writeSerialPort("Y04\0", 4);
										}
										else if (sy < 3.0) {
										yc = 5;
										arduino.writeSerialPort("Y05\0", 4);
										}
										else if (sy < 3.5) {
										yc = 6;
										arduino.writeSerialPort("Y06\0", 4);
										}
										else if (sy < 4.0) {
										yc = 7;
										arduino.writeSerialPort("Y07\0", 4);
										}
										else if (sy < 4.5) {
										yc = 8;
										arduino.writeSerialPort("Y08\0", 4);
										}
										else if (sy < 5.0) {
										yc = 9;
										arduino.writeSerialPort("Y09\0", 4);
										}
										else if (sy < 5.5) {
										yc = 10;
										arduino.writeSerialPort("Y10\0", 4);
										}
										else if (sy < 6.0) {
										yc = 11;
										arduino.writeSerialPort("Y11\0", 4);
										}
										else if (sy < 6.5) {
										yc = 12;
										arduino.writeSerialPort("Y12\0", 4);
										}
										else if (sy < 7.0) {
										yc = 13;
										arduino.writeSerialPort("Y13\0", 4);
										}
										else if (sy < 7.5) {
										yc = 14;
										arduino.writeSerialPort("Y14\0", 4);
										}
										else {
										yc = 15;
										arduino.writeSerialPort("Y15\0", 4);
										}

										std::printf("  POS: out <%d,%d>  \n", xc, yc);
										*/
									}
									else {
										std::printf("ERR: Arduino Not Found...\n");
									}
								}
							}
						}
					}
					//}

				}
			}
			count_backoff++;
			if (count_backoff > 5) count_backoff = 0;

			if (g_timer)
			{
				timer.Tick();
			}

			g_senseManager->ReleaseFrame();
		} // end while acquire frame

	} // end if Init

	else
	{
		releaseAll();
		std::printf("Failed Initializing PXCSenseManager\n");
		return;
	}


	releaseAll();
}

void releaseAll()
{

	if (g_handConfiguration)
	{
		g_handConfiguration->Release();
		g_handConfiguration = NULL;
	}
	if (g_cursorConfiguration)
	{
		g_cursorConfiguration->Release();
		g_cursorConfiguration = NULL;
	}

	if (g_handDataOutput)
	{
		g_handDataOutput->Release();
		g_handDataOutput = NULL;
	}
	if (g_cursorDataOutput)
	{
		g_cursorDataOutput->Release();
		g_cursorDataOutput = NULL;
	}


	if (g_senseManager)
	{
		g_senseManager->Close();
		g_senseManager->Release();
		g_senseManager = NULL;
	}
	if (g_session)
	{
		g_session->Release();
		g_session = NULL;
	}
}

